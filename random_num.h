#ifndef RANDOM_NUM_H
#define RANDOM_NUM_H

// random_shuffle example
#include <iostream>     // std::cout
#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand

// random generator function:
int myrandom (int i) { return std::rand()%i;}

std::vector<int> rand_generator(int size)
{
  std::srand ( unsigned ( std::time(0) ) );
  std::vector<int> myvector;

  // set some values:
  for (int i=0; i<size; ++i) myvector.push_back(i); // 0 1 2 3 4 5 6 7 8 .. size-1

  // using built-in random generator:
  std::random_shuffle ( myvector.begin(), myvector.end() );

  // using myrandom:
  std::random_shuffle ( myvector.begin(), myvector.end(), myrandom);

  // print out content:
  //std::cout << "myvector contains:";
  //for (std::vector<int>::iterator it=myvector.begin(); it!=myvector.end(); ++it)
    //std::cout << ' ' << *it;

  //std::cout << '\n';
  
  return myvector;
}

#endif
