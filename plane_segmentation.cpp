// 2015 author: Arsad, Derza Anjasa
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <cv.h>
#include "random_num.h"
#define ransac_iterations 20
#include <fstream>

using namespace cv;
using namespace std;
double get_angle(const Vec3b& vec_ref, const Vec3b& vec_test)
{
    double dot_product = (vec_ref.val[0] * vec_test.val[0]) + (vec_ref.val[1] * vec_test.val[1]) + (vec_ref.val[2] * vec_test.val[2]);
    double divider = sqrt(pow(vec_ref.val[0], 2.0) + pow(vec_ref.val[1], 2.0) + pow(vec_ref.val[2], 2.0)) * sqrt(pow(vec_test.val[0], 2.0) + pow(vec_test.val[1], 2.0) + pow(vec_test.val[2], 2.0));
    double cos_phi = dot_product/divider;
    
    double degree = acos(fmin(fmax(cos_phi,-1.0),1.0)) * 180.0 /M_PI;
    
    return degree;
}

void ransac_from_points(vector<Point>& points, const Mat& src, Mat& dst, const double& n_samples, const double& ransac_threshold)
{
    // random generator
    vector<int> indexPoint = rand_generator(points.size());
    
    // RANSAC
    Point best_point_ref = Point(0,0);
    
    double ratio = 0.;
    vector<Point> inliers;
    vector<Point> inliers_list;
    vector<Point> outliers;
    vector<Point> outliers_list;
    for(int i=0;i<ransac_iterations;i++)
    {
      // pick up one random points
      Point point_ref = points[indexPoint[i]];
    
      // define reference vector
      Vec3b vec_ref = src.at<Vec3b>(point_ref);
    
      int num = 0;
      inliers_list.clear();
      outliers_list.clear();
      for(int j=0;j<indexPoint.size();j++)
      {
        Point point_test = points[indexPoint[j]];
        if(point_test != point_ref) // ratio will never be 1.0 because the reference point does not do self comparison
        {
          Vec3b vec_test = src.at<Vec3b>(point_test);
          
          // check whether it's an inlier or not
          if(get_angle(vec_ref, vec_test) < ransac_threshold)
          {
            inliers_list.push_back(point_test);
            num += 1;
          }
          else
            outliers_list.push_back(point_test);
        }
      }
      
      // in case a new model is better - cache it
      if (((double)num/(n_samples)) > ratio)
      {
        best_point_ref = point_ref;
      
        ratio = (double)num/(n_samples);
        inliers.clear();
        inliers = inliers_list;
        outliers.clear();
        outliers = outliers_list;
      }
    }
    
    printf("best ratio = %f\n", ratio);
    
    //change all pixel
    Vec3b color = src.at<Vec3b>(best_point_ref);
    for(int i=0;i<inliers.size();i++) dst.at<Vec3b>(inliers[i]) = color;
    
    points.clear();
    points = outliers;
}

static void draw_subdiv( cv::Mat& img, cv::Subdiv2D& subdiv, cv::Scalar delaunay_color, const float alpha, vector<Point>& borders)
{
  cv::Scalar fill_color(0,153,0); // BGR
  borders.clear();
  cv::Rect rect(0, 0, img.cols, img.rows);
  cv::Subdiv2D subdiv_white(rect);

  std::vector<cv::Vec6f> triangleList;
  subdiv.getTriangleList(triangleList);
  std::vector<cv::Point> pt(3);

  float max_radius = img.rows > img.cols ? img.rows : img.cols;
  float a, b, c, s, area, circum_r;
  for( size_t i = 0; i < triangleList.size(); i++ )
  {
    cv::Vec6f t = triangleList[i];
    pt[0] = cv::Point(cvRound(t[0]), cvRound(t[1]));
    pt[1] = cv::Point(cvRound(t[2]), cvRound(t[3]));
    pt[2] = cv::Point(cvRound(t[4]), cvRound(t[5]));

    // Alpha Shape
    // Lengths of sides of triangle
    a = sqrt((pt[0].x-pt[1].x)*(pt[0].x-pt[1].x) + (pt[0].y-pt[1].y)*(pt[0].y-pt[1].y));
    b = sqrt((pt[1].x-pt[2].x)*(pt[1].x-pt[2].x) + (pt[1].y-pt[2].y)*(pt[1].y-pt[2].y));
    c = sqrt((pt[2].x-pt[0].x)*(pt[2].x-pt[0].x) + (pt[2].y-pt[0].y)*(pt[2].y-pt[0].y));

    // Semiperimeter of triangle
    s = (a + b + c)/2.0;

    // Area of triangle by Heron's formula
    area = sqrt(s*(s-a)*(s-b)*(s-c));

    circum_r = a*b*c/(4.0*area);

    // Here's the radius filter.
    if (circum_r < max_radius/alpha)
    {
      /*cv::line(img, pt[0], pt[1], delaunay_color, 1, cv::LINE_AA, 0);
      cv::line(img, pt[1], pt[2], delaunay_color, 1, cv::LINE_AA, 0);
      cv::line(img, pt[2], pt[0], delaunay_color, 1, cv::LINE_AA, 0);*/

      // fill the triangle
      cv::Point tri_points[1][3];
      tri_points[0][0] = pt[0];
      tri_points[0][1] = pt[1];
      tri_points[0][2] = pt[2];

      const cv::Point* ppt[1] = { tri_points[0] };
      int npt[] = { 3 };

      cv::fillPoly( img, ppt, npt, 1, fill_color, 8 );
    }
    else
    {
      if((abs(pt[0].x) < img.cols) && (abs(pt[1].x) < img.cols) && (abs(pt[2].x) < img.cols) &&
         (abs(pt[0].y) < img.rows) && (abs(pt[1].y) < img.rows) && (abs(pt[2].y) < img.rows)
         )
      {
        subdiv_white.insert(pt[0]);
        subdiv_white.insert(pt[1]);
        subdiv_white.insert(pt[2]);

        /*circle(img, pt[0], 1, Scalar(), FILLED, LINE_AA, 0);
        circle(img, pt[1], 1, Scalar(), FILLED, LINE_AA, 0);
        circle(img, pt[2], 1, Scalar(), FILLED, LINE_AA, 0);*/

        borders.push_back(pt[0]);
        borders.push_back(pt[1]);
        borders.push_back(pt[2]);
      }
    }
  }

  subdiv = subdiv_white;
}

void alpha_shape( cv::Mat& plane_img, cv::Subdiv2D& subdiv, const float alpha, vector<Point>& borders)
{
  cv::Scalar delaunay_color(153,0,0); // BGR

  // Fill rectangle
  cv::Vec3b white = cv::Vec3b(255,255,255);
  for( int indexCol = 0; indexCol < plane_img.cols; indexCol++ )
  {
    for( int indexRow = 0; indexRow < plane_img.rows; indexRow++ )
    {
      cv::Point current_point = cv::Point(indexCol, indexRow);
      cv::Vec3b current_colour = plane_img.at<cv::Vec3b>(current_point);
      if(current_colour != white)
      {
        cv::Point2f fp( (float)current_point.x, (float)current_point.y);
        subdiv.insert(fp);
      }
    }
  }

  draw_subdiv( plane_img, subdiv, delaunay_color, alpha, borders);
}

void draw_from_points(cv::Mat& img, const vector<Point>& borders)
{
  cv::Scalar fill_color(0,153,0); // BGR
  cv::Point border_points[1][borders.size()];

  for( int i = 0; i < borders.size(); i++ )
  {
    border_points[0][i] = borders[i];

    // draw points
    circle(img, borders[i], 1, Scalar(), FILLED, LINE_AA, 0);
  }

  const cv::Point* ppt[1] = { border_points[0] };
  int npt[] = { borders.size() };

  //cv::fillPoly( img, ppt, npt, 1, fill_color, 8 );
}


int main(int argc, const char * argv[])
{
    if(argc != 4) {
      cout << "You need to supply three argument to this program.";
      return -1;
    }
    // load the input image  
    Mat src = imread( argv[1], CV_BGR2RGB );
    
    //plane segmentation start
    Mat dst;
    src.copyTo(dst);
    
    //  Ransac parameters
    double ransac_threshold = atof(argv[2]);    // threshold
    double n_samples = src.total();

    // random generator
    vector<int> indexCol = rand_generator(src.cols);
    vector<int> indexRow = rand_generator(src.rows);
    
    
    // RANSAC
    Point best_point_ref = Point(0,0);
    
    double ratio = 0.;
    vector<Point> inliers;
    vector<Point> inliers_list;
    vector<Point> outliers;
    vector<Point> outliers_list;
    for(int i=0;i<ransac_iterations;i++)
    {
      // pick up one random points
      int x_ref = indexCol[i];
      int y_ref = indexRow[i];
      Point point_ref = Point(x_ref, y_ref);
    
      // define reference vector
      Vec3b vec_ref = src.at<Vec3b>(point_ref);
    
      int num = 0;
      inliers_list.clear();
      outliers_list.clear();
      for(int y=0;y<src.rows;y++)
      {
        for(int x=0;x<src.cols;x++)
        {
          Point point_test = Point(x, y);
          if(point_test != point_ref) // ratio will never be 1.0 because the reference point does not do self comparison
          {
            Vec3b vec_test = src.at<Vec3b>(point_test);
            
            // check whether it's an inlier or not
            if(get_angle(vec_ref, vec_test) < ransac_threshold)
            {
              inliers_list.push_back(point_test);
              num += 1;
            }
            else
            outliers_list.push_back(point_test);
          }
        }
      }
      
      // in case a new model is better - cache it
      if (((double)num/(n_samples)) > ratio)
      {
        best_point_ref = point_ref;
      
        ratio = (double)num/(n_samples);
        inliers.clear();
        inliers = inliers_list;
        outliers.clear();
        outliers = outliers_list;
      }
    }
    
    printf("best ratio = %f\n", ratio);
    
    /*ransac_from_points(outliers, src, dst, n_samples, ransac_threshold);
    ransac_from_points(outliers, src, dst, n_samples, ransac_threshold);
    ransac_from_points(outliers, src, dst, n_samples, ransac_threshold);
    ransac_from_points(outliers, src, dst, n_samples, ransac_threshold);
    ransac_from_points(outliers, src, dst, n_samples, ransac_threshold);*/

  
    
    //change all pixel
    Vec3b color = src.at<Vec3b>(best_point_ref);
    Vec3b outcolor = Vec3b(255,255,255);

    /*ofstream outputFile;

    outputFile.open("planePoints.txt");
    std::cout << dst.rows << std::endl;
    std::cout << dst.cols << std::endl;
    float max_size = dst.rows > dst.cols ? dst.rows : dst.cols;
    std::cout << max_size << std::endl;
    for(int i=0;i<inliers.size();i++)
    {
      dst.at<Vec3b>(inliers[i]) = color;

      float x_in = (float)inliers[i].x;
      float y_in = (float)inliers[i].y;

      x_in /= max_size;
      y_in /= max_size;

      x_in *= 3.0f;
      y_in *= 3.0f;

      x_in -= 1.0f;
      y_in -= 1.0f;
      outputFile << x_in << " ";
      outputFile << y_in << endl;
    }
    outputFile.close();*/

    for(int i=0;i<outliers.size();i++) dst.at<Vec3b>(outliers[i]) = outcolor;
      
    // draw a blue circle at the rand point
    //circle(dst, best_point_ref, 3, Scalar(255,0,0), 3, 8);
  
    // show the output
    imshow( "Original", src );
    imshow( "Plane", dst );

    // Do delaunay triangulation and alpha shape
    Mat alpha_dst;
    dst.copyTo(alpha_dst);

    cv::Rect rect(0, 0, alpha_dst.cols, alpha_dst.rows);
    cv::Subdiv2D subdiv(rect);
    vector<Point> borders;
    alpha_shape(alpha_dst, subdiv, atof(argv[3]), borders);
    alpha_dst = Scalar::all(255);
    draw_from_points(alpha_dst, borders);
    circle(alpha_dst, best_point_ref, 3, Scalar(255,0,0), FILLED, LINE_AA, 0);

    imshow( "Alpha", alpha_dst );
  
    // wait for user  
    waitKey( 0 ); 
  
    // save image  
    imwrite("output.png", dst);
  
    return 0;  
}

